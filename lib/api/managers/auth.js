//----------------------------------------------------------------------------------------------------------------------
// AuthManager
//----------------------------------------------------------------------------------------------------------------------

import { v4 } from 'uuid';

// ResourceAccess
import gitlabRA from '../resource-access/gitlab';

//----------------------------------------------------------------------------------------------------------------------

class AuthManager
{
    constructor()
    {
        this._account = undefined;
        this._token = undefined;
        this._state = window.localStorage.getItem('oauth_state') || v4();
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------
    // Properties
    //------------------------------------------------------------------------------------------------------------------

    get account(){ return this._account; }
    get token(){ return this._token; }
    get state(){ return this._state; }

    //------------------------------------------------------------------------------------------------------------------
    // Methods
    //------------------------------------------------------------------------------------------------------------------

    regenState()
    {
        this._state = v4();
        window.localStorage.setItem('oauth_state', this._state);
        return this._state;
    } // end regenState

    async setCode(access_token, token_type, expires_in)
    {
        this._token = {
            access_token,
            token_type,
            expires_in
        };

        // Set the token
        gitlabRA.setToken(access_token);

        // Set the current user.
        this._account = await gitlabRA.api.Users.current();
    } // end setCode
} // end AuthManager

//----------------------------------------------------------------------------------------------------------------------

export default new AuthManager();

//----------------------------------------------------------------------------------------------------------------------
