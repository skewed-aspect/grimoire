# Grimoire

Grimoire is a "faux-headless" CMS powered by a very special blend of technology:

* [VueJS][vue]
* [Markdown-it][markdown-it]
* [Bootstrap][bootstrap]
* [SaberJS][saber]
* [GitLab][gitlab]

[vue]: https://vuejs.org/
[markdown-it]: https://markdown-it.github.io/
[bootstrap]: https://getbootstrap.com/
[saber]: https://saber.land/
[gitlab]: https://about.gitlab.com/

Each and every technology was picked to make Grimoire as easy, powerful and customizable as possible. While each of those technologies are great, they blend together perfectly to build something greater than the sum of it's parts.

## Features

* Write pages in either [markdown][markdown-it] or [vue][].
* All pages are revision controlled in git.
* OAuth Web-flow powered by [GitLab][gitlab].
* Page editing powered by [GitLab][gitlab] api.
* Built in components:
    * Simple Pages
    * Wiki (page history, comments, and search)
    * Blog (multi-user, read-more, and slug-based urls)
* Fully integrated Continuous Integration, with automatic publishing.
* Completely static, 100% cache-able, regenerated on demand.

## What is a "faux-headless" CMS?

Well, Grimoire is.

More specifically, however, Grimoire is not truly "headless", because it does have a server powering it, and acting as it's database; GitLab's api powers many of the more advanced features. Those features require the GitLab api to be able to use it's database to answer questions (for example, listing all the revisions for a given page). If GitLab were to go down, (and you had a service like CloudFlare's CDN powering your site) those features would stop working, even though the main content of your page would continue to be served.

In this way, Grimoire does not behave like a static site; simply having the compiled output isn't enough for it's features to work, unlike traditional "headless" CMS solutions. However, it's not a traditional CMS, in that there's no server to host. GitLab does all the hard work for you, including serving your static files.
