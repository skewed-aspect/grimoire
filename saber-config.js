//----------------------------------------------------------------------------------------------------------------------
// DocsConfig
//----------------------------------------------------------------------------------------------------------------------

module.exports = {
    siteConfig: {
        title: 'Grimoire CMS',
        lang: 'en',
        auth: {
            appID: '070369e8b144ae61f209936b03e9b1c1373d8a88b1053c585503cf4d45c023e3'
        }
    },
    theme: './theme',
    themeConfig: {
        bootswatch: 'flatly',
        navLinks: [
            {
                text: 'Guide',
                to: '/guide',
                position: 'right'
            },
            {
                text: 'API',
                to: '/api',
                position: 'right'
            }
        ],
        footerLinks: [
            {
                text: 'GitLab Project',
                href: 'https://gitlab.com/skewed-aspect/grimoire',
                icon: [ 'fab', 'gitlab' ]
            },
            {
                text: 'About Us',
                href: 'https://skewedaspect.com',
                icon: 'question'
            }
        ]
    },
    markdown: {
        headings: {
            permalink: true
        }
    },
    build: {
        // publicUrl: '/saber-theme-armdocs',
    }
};

//----------------------------------------------------------------------------------------------------------------------
