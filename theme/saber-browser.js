//----------------------------------------------------------------------------------------------------------------------
// Saber Browser API
//----------------------------------------------------------------------------------------------------------------------

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

//-----------------------------------------------------------------------------------------------------------------------
// Site Theme
//-----------------------------------------------------------------------------------------------------------------------

// Change this theme to match the bootswatch theme you'd prefer
import 'bootswatch/dist/flatly/bootstrap.min.css';

// Keep this for custom overrides
import './scss/theme.scss';

//-----------------------------------------------------------------------------------------------------------------------
// Vue Bootstrap
//-----------------------------------------------------------------------------------------------------------------------

import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue);

//----------------------------------------------------------------------------------------------------------------------
// Font Awesome
//----------------------------------------------------------------------------------------------------------------------

// Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon, FontAwesomeLayers } from '@fortawesome/vue-fontawesome';

library.add(fab, fas);
Vue.component('fa', FontAwesomeIcon);
Vue.component('fa-layers', FontAwesomeLayers);

//----------------------------------------------------------------------------------------------------------------------
